<?php
/*
*
* @category       Levosoft
* @package        Levosoft_CustomSearch
* @copyright      Levosoft (c) 2019
* @Author         Hassan Ali Shahzad
* @email          levosoft786@gmail.com
* @date           05-05-2019 13:19
*
 */

namespace Levosoft\CustomSearch\Controller\Vehicle;

require_once (BP.'/lib/internal/simpleHtmlDom/simple_html_dom.php');

class Getresults extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $curlClient;
    protected $tyresOnLineHelper;


    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Levosoft\CustomSearch\Helper\Tyresonline $tyresonlinehelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->curlClient = $curl;
        $this->tyresOnLineHelper = $tyresonlinehelper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $serviceUrl = "https://www.tyresonline.ae/productsearch/vehicle/index";
        $response = "";

        try {
            if(!empty($post['vehicle']) && !empty($post['vehicle_model']) && !empty($post['vehicle_year'])) {
                $this->curlClient->post($serviceUrl, $post);
                $html = str_get_html($this->curlClient->getBody());
                //echo $html;
                if (!empty($html)) {
                    //echo "haaha";
                    foreach ($html->find('.wheel-font') as $element) {
                        //$html->dump_node($element);
                        $tyresOnlineSizes[] = $element->plaintext;

                    }
                }
                if(!empty($tyresOnlineSizes))
                    $response =$this->tyresOnLineHelper->processSizes($tyresOnlineSizes);

            }
            return $this->jsonResponse($response);
            //var_dump($response);exit;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            //$this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
