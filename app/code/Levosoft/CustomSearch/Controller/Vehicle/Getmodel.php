<?php

/**
 *
 * @category       Levosoft
 * @package        Levosoft_CustomSearch
 * @copyright      Levosoft (c) 2019
 * @Author         Hassan Ali Shahzad
 * @email          levosoft786@gmail.com
 * @date           05-05-2019 13:19
 *
 */

namespace Levosoft\CustomSearch\Controller\Vehicle;

class Getmodel extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $curlClient;


    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->curlClient = $curl;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $serviceUrl = "https://www.tyresonline.ae/productsearch/vehicle/getmodeldata/";
        $response = "";

        try {
            if(!empty($post['make'])){
                $this->curlClient->post($serviceUrl, $post);
                $response = $this->curlClient->getBody();
            }

            echo $response;
            exit;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
