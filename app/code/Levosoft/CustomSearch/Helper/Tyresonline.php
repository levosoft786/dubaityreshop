<?php

/**
 *
 * @category       Levosoft
 * @package        Levosoft_CustomSearch
 * @copyright      Levosoft (c) 2019
 * @Author         Hassan Ali Shahzad
 * @email          levosoft786@gmail.com
 * @date           05-05-2019 13:19
 *
 */
namespace Levosoft\CustomSearch\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Tyresonline extends AbstractHelper
{

    protected $eavConfig;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Eav\Model\Config $eavConfig
    ) {
        $this->eavConfig = $eavConfig;
        parent::__construct($context);
    }

    /**
     * This function will get all sizes from tyresonline site and return only those combinations which exist in our site
     * e.g input array array(62) {[0]=>string(9) "265/40R20" [1]=>string(9) "275/35R21"}
     *
     * @param $tyresOnlineSizes
     * @return array
     */
    public function processSizes($tyresOnlineSizes){

        $allTyreWidths   = $this->getCustomOption('tyre_width');
        $allTyreHeights  = $this->getCustomOption('tyre_height');
        $allTyreRimSizes = $this->getCustomOption('tyre_rim_size');
        $availableSizes = [];

        foreach ($tyresOnlineSizes as $tyresOnlineSize){
            $temp = explode('/',$tyresOnlineSize);
            $tyreWidth = $temp[0];

            $temp1 = explode('R',$temp[1]);
            $tyreHeight = $temp1[0];
            $tyreRimSize = "R".$temp1[1];
            unset($temp);
            unset($temp1);
            if(array_key_exists($tyreWidth,$allTyreWidths) && array_key_exists($tyreHeight,$allTyreHeights) && array_key_exists($tyreRimSize,$allTyreRimSizes)){
                $availableSizes[$tyresOnlineSize] = "/catalogsearch/advanced/result/?tyre_width={$allTyreWidths[$tyreWidth]}&tyre_height={$allTyreHeights[$tyreHeight]}&tyre_rim_size={$allTyreRimSizes[$tyreRimSize]}";
            }

        }
            return $availableSizes;

    }


    /**
     * @return string
     */
    public function getCustomOption($attributeCode)
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', $attributeCode);
        $options = $attribute->getSource()->getAllOptions();

        $sortedOption = [];
        foreach($options as $option){
            if(!empty($option['value']) && !empty($option['label'])){
                $sortedOption[$option['label']] = $option['value'];
            }
        }
        ksort($sortedOption);
        return $sortedOption;
    }
    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }
}
