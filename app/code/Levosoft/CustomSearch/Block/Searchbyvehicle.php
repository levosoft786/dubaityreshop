<?php

/**
 *
 * @category       Levosoft
 * @package        Levosoft_CustomSearch
 * @copyright      Levosoft (c) 2019
 * @Author         Hassan Ali Shahzad
 * @email          levosoft786@gmail.com
 * @date           05-05-2019 13:19
 *
 */

namespace Levosoft\CustomSearch\Block;

class Searchbyvehicle extends \Magento\Framework\View\Element\Template
{

    protected $eavConfig;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Eav\Model\Config $eavConfig,
        array $data = []
    )
    {
        $this->eavConfig = $eavConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getCustomOption($attributeCode)
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', $attributeCode);
        $options = $attribute->getSource()->getAllOptions();

        $sortedOption = [];
        foreach($options as $option){
            if(!empty($option['value']) && !empty($option['label'])){
                $sortedOption[$option['label']] = $option['value'];
            }
        }
        ksort($sortedOption);
        return $sortedOption;
    }
}
