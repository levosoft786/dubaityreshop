require(['jquery'],function($){



    $(document).ready(function(){

        $('#advsearchbtn').click(function(e){
            e.preventDefault(e);
            var oneFilled = checkFields($('#form-validate'));
            if(oneFilled){
                $('#form-validate').find('select,input').each(function(index, obj){
                    console.log(obj);
                    if($(obj).val() == "") {
                        $(obj).attr("disabled", true);
                    }
                });
                $('#form-validate').submit();
            }
            else {
                alert('Please specify at least one search term for Search By Size');
            }

        });




        $(document).on('change','#vehicle',function(){
            var wheelModel_url = window.location.href+"searchbyvehicle/vehicle/getmodel/";
            var selected_make = $(this).val();
            jQuery('#vehiclesearchresults').fadeOut()
            $.ajax({
                type: 'POST',
                showLoader: true,
                url: wheelModel_url,
                data: "make=" + selected_make,
                success: function (resultData) {
                    //$('#vehicle_model').html(resultData.response);
                    //console.log(resultData);
                    var obj = $.parseJSON( resultData );
                    //console.log(obj.response);
                    if(obj.response != null)
                        $('select[name="vehicle_model"]').html(obj.response);
                    //$('#loading-mask , #loading_mask_loader').hide();
                    $('#vehicle_model').prop('disabled', false);
                }
            });
        });


        $(document).on('change','#vehicle_model',function(){

            var model = $(this).val();
            var make = $('#vehicle').find(":selected").val();
            var getmodelyear_url = window.location.href+"searchbyvehicle/vehicle/getyear/";
            jQuery('#vehiclesearchresults').fadeOut();

            $.ajax({
                type: 'POST',
                showLoader: true,
                url: getmodelyear_url,
                data: {make: make, model: model},
                success: function (resultData) {
                    var obj = $.parseJSON( resultData );
                    $('select[name="vehicle_year"]').html(obj.response);
                    $('#loading-mask , #loading_mask_loader').hide();
                    $('#vehicle_year').prop('disabled', false);
                }
            });
        });


        $(document).on('click','#vehiclesearchbtn',function(){

            var make   =  $('#vehicle').find(":selected").val();
            var model  =  $('#vehicle_model').find(":selected").val();
            var year   =  $('#vehicle_year').find(":selected").val();
            var index_url = window.location.href+"searchbyvehicle/vehicle/getresults/";
            jQuery('#vehiclesearchresults').fadeOut()
            if(make == ""){
                //$('#vehicle').css( "border-color", "red" );
                var $el = $("#vehicle"),
                    x = 2000,
                    originalColor = $el.css("border-color");

                $el.css("border-color", "red");
                setTimeout(function(){
                    $el.css("border-color", originalColor);
                }, x);
            }
            else if(model == ""){
                //$('#vehicle_model').css( "border-color", "red" );
                var $el = $("#vehicle_model"),
                    x = 2000,
                    originalColor = $el.css("border-color");

                $el.css("border-color", "red");
                setTimeout(function(){
                    $el.css("border-color", originalColor);
                }, x);
            }
            else if(year == ""){
                //$('#vehicle_year').css( "border-color", "red" );
                var $el = $("#vehicle_year"),
                    x = 2000,
                    originalColor = $el.css("border-color");

                $el.css("border-color", "red");
                setTimeout(function(){
                    $el.css("border-color", originalColor);
                }, x);
            }
            else {
                    $.ajax({
                        type: 'POST',
                        showLoader: true,
                        url: index_url,
                        data: {vehicle: make, vehicle_model: model, vehicle_year: year},
                        success: function (resultData) {
                            var table = $("#vehiclesearchresults-table tbody");
                            var counter = 0;
                            $.each(resultData, function (i,v)
                            {
                                table.append("<tr><th scope=\"row\">"+i+"</th><td><div class=\"wheel-font-btn\"><a class=\"\" href=\""+v+"\">Shop this Size</a></div></td>");
                                counter++;
                            });
                            if(counter > 0)
                                $('#vehiclesearchresults').slideToggle();

                        }
                    });
            }
        });


    });

    /**
     * 1) gather all checkboxes and radio buttons
     * 2) gather all inputs that are not checkboxes or radios, and are not buttons (submit/button/reset)
     * 3) get only those checkboxes and radio buttons that are checked
     * 4) get only those field elements that have a value (spaces get trimmed)
     * 5) if the length of both resulting collections is zero, nothing has been filled out
     */
    function checkFields(form) {
        var checks_radios = form.find(':checkbox, :radio'),
            inputs = form.find(':input').not(checks_radios).not('[type="submit"],[type="button"],[type="reset"]'),
            checked = checks_radios.filter(':checked'),
            filled = inputs.filter(function(){
                return $.trim($(this).val()).length > 0;
            });

        if(checked.length + filled.length === 0) {
            return false;
        }

        return true;
    }
});