## Base Magento 2 Extension

By: CynoInfotech Team
Platform: Magento 2 Community Edition
Email: cynoinfotech@gmail.com

###1 - Installation Guide

 * Download extension zip file from the account
 * Unzip the file
 * root of your Magento 2 installation and merge app folder {Magento Root}
 * folder structure --> app/code/Cynoinfotech/Base

####2 -  Enable Extension :  
  
  After this run below commands from Magento 2 root directory to install module.
  
 * Run :cd < your Magento install dir >
 * php bin/magento cache:disable
 * php bin/magento module:enable Cynoinfotech_Base
 * php bin/magento setup:upgrade
 * php bin/magento setup:static-content:deploy
 * rm -rf var/cache var/generation var/di var/cache  /generated

###3 Cynoinfotech_Base only supporting extension


###4 To request support:

Feel free to contact us via email: cynoinfotech@gmail.com

www.cynoinfotech.com