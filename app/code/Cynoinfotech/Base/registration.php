<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_Base
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cynoinfotech_Base',
    __dir__
);
