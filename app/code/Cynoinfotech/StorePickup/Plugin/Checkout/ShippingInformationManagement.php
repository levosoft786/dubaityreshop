<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_StorePickup
 */
namespace Cynoinfotech\StorePickup\Plugin\Checkout;

class ShippingInformationManagement
{
    protected $storepickupHelper;

    public function __construct(
        \Cynoinfotech\StorePickup\Helper\Data $storepickupHelper
    ) {
        $this->storepickupHelper = $storepickupHelper;
    }

    public function aroundSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        \Closure $proceed,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        if ($extAttributes instanceof \Magento\Checkout\Api\Data\ShippingInformationExtension) {
            $data = [];
            if ($extAttributes->getStorepickupShippingChecked()) {
                $data = [
                    'store_pickup'         => $extAttributes->getStorePickup(),
                    'calendar_inputField'  => $extAttributes->getCalendarInputField()
                ];
            }
                $this->storepickupHelper->setStorepickupDataToSession($data);
        }
        
        return $proceed($cartId, $addressInformation);
    }
}
