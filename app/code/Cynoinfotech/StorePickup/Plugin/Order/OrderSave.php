<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_StorePickup
 */
namespace Cynoinfotech\StorePickup\Plugin\Order;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Session\Storage;

class OrderSave
{
    protected $helper;
    protected $storepickuporderFactory;
    protected $objectManager = null;
    protected $sessionStorage;

    public function __construct(
        \Cynoinfotech\StorePickup\Helper\Data $helper,
        \Cynoinfotech\StorePickup\Model\StorePickupOrderFactory $storepickuporderFactory,
        \Cynoinfotech\StorePickup\Model\StorePickupFactory $storepickupFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Storage $sessionStorage
    ) {
        $this->helper = $helper;
        $this->storepickuporder = $storepickuporderFactory;
        $this->storepickupFactory = $storepickupFactory;
        $this->objectManager = $objectManager;
        $this->sessionStorage = $sessionStorage;
    }

    public function afterSave(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $data = $this->helper->getStorepickupDataFromSession();
		
        if(!empty($data['store_pickup'])){
		
			$orderIncrementId = $order->getIncrementId();
			$orderId = $order->getId();
			$store_pickup_id = $data['store_pickup'];
			
			if (is_array($data) && !($data === null)) {
				$data['order_id'] = $orderId;
				
				$pickup_address = $this->getPickupAddress($store_pickup_id);
				$data['pickup_address'] = $pickup_address;
				
				$obj = $this->objectManager->get('\Cynoinfotech\StorePickup\Model\ResourceModel\StorePickupOrder');
				$obj->SavePickupOrder($data);
				$this->sessionStorage->unsData($this->helper->getStorepickupAttributesSessionKey());
			}			
		}
		
		return $order;
    }
    
    public function getPickupAddress($id)
    {
        $storepickup = $this->storepickupFactory->create();
        $pickup_address = $storepickup->getCollection()->addFieldToFilter('entity_id', ['eq' => $id]);
        
        $full_address = '';
        if (!empty($pickup_address->getData())) {
            $full_address.= $pickup_address->getData()[0]['name'] . ', ';
            $full_address.= $pickup_address->getData()[0]['store_address'] . ', ';
            $full_address.= $pickup_address->getData()[0]['store_city'] . ', ';
            $full_address.= $pickup_address->getData()[0]['store_state'] . ', ';
            $full_address.= $pickup_address->getData()[0]['store_pincode'] . ', ';
            $full_address.= $pickup_address->getData()[0]['store_country'] . ', ';
            $full_address.= 'T: ' . $pickup_address->getData()[0]['store_phone'] . ', ';
            $full_address.= 'Email: ' . $pickup_address->getData()[0]['store_email'];
        }
         
        return $full_address;
    }
}
