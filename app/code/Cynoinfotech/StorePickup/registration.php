<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_StorePickup
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cynoinfotech_StorePickup',
    __dir__
);
