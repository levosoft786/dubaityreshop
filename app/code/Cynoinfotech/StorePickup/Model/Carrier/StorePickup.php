<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_StorePickup
 */
namespace Cynoinfotech\StorePickup\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Psr\Log\LoggerInterface;

class StorePickup extends AbstractCarrier implements CarrierInterface
{
    /**
     * Constant for method code
     */
    const METHOD_CODE = 'cistorepickup';
    
    protected $_code = self::METHOD_CODE;
    protected $_isFixed = true;
    protected $rateResultFactory;
    protected $rateMethodFactory;
    
    /**
     * Carrier constructor
     *
     * @param ScopeConfigInterface $scopeConfig       Scope Configuration
     * @param ErrorFactory         $rateErrorFactory  Rate error Factory
     * @param LoggerInterface      $logger            Logger
     * @param ResultFactory        $rateResultFactory Rate result Factory
     * @param MethodFactory        $rateMethodFactory Rate method Factory
     * @param array                $data              Carrier Data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }
    /**
     * {@inheritdoc}
     */
    public function getAllowedMethods()
    {
        return [$this->getCarrierCode() => __($this->getConfigData('name'))];
    }
    /**
     * {@inheritdoc}
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();
        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->rateMethodFactory->create();
        $method->setCarrier($this->getCarrierCode());
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->getCarrierCode());
        $method->setMethodTitle($this->getConfigData('name'));
        $amount = $this->getConfigData('price');
        $price = $this->getFinalPriceWithHandlingFee($amount);
        $method->setPrice($price);
        $method->setCost($price);
        $result->append($method);
        return $result;
    }
}
