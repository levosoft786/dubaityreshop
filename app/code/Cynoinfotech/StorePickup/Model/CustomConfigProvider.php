<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_StorePickup
 */
 
namespace Cynoinfotech\StorePickup\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class CustomConfigProvider implements ConfigProviderInterface
{
    protected $storeManager;
    protected $scopeConfig;
    
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Cynoinfotech\StorePickup\Model\StorePickupFactory $storepickupFactory,
        \Cynoinfotech\StorePickup\Helper\Data $dataHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->dataHelper = $dataHelper;
        $this->storepickup = $storepickupFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function getConfig()
    {
        $storepick_config = [];
        $storepick_info = [];
        $storepick_location = [];
        $apikey = $this->dataHelper->getApiKey();
        $hour_min = $this->dataHelper->getPickupHoursStart();
        $hour_max = $this->dataHelper->getPickupHoursEnd();
        $zoom_level = $this->dataHelper->getMapZoomLevel();
        
        $store_lat = $this->dataHelper->getStoreLat();
        $store_lng = $this->dataHelper->getStoreLng();
        
        $storepickup = $this->storepickup->create();
        $collection = $storepickup->getCollection();
        foreach ($collection as $item) {
            $storepick_config[] = [
                                    'value' => $item->getData('entity_id'),
                                    'label' => $item->getData('name'). ' - '. $item->getData('store_address').', '.$item->getData('store_city').', '.$item->getData('store_country')
                                   ];
            
            $storepick_info[] = ['value' => 'store_info_'.$item->getData('entity_id'),
                'label' => $item->getData('name').', '.
                $item->getData('store_address').', '.
                $item->getData('store_city').', '.
                $item->getData('store_state').',-'.
                $item->getData('store_pincode').', '.
                $item->getData('store_country').',  T:'.
                $item->getData('store_phone').', email: '.
                $item->getData('store_email')];
                
            $storepick_location[] =[$item->getData('name'),
                $item->getData('store_latitude'),
                $item->getData('store_longitude')
            ];
        }
        
        $config = [
            'storepick_config' => $storepick_config,
            'storepick_info' => $storepick_info,
            'storepick_location' => $storepick_location,
            'apikey' => $apikey,
            'hour_min' => $hour_min,
            'hour_max' => $hour_max,
            'zoom_level' => $zoom_level,
            'store_lat' => $store_lat,
            'store_lng' => $store_lng,
            'storepick_config_encode' => json_encode($storepick_config),
        ];
        return $config;
    }
}
